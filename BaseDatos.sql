CREATE DATABASE ProyectoIPC
USE ProyectoIPC

CREATE TABLE ROL(
		id_rol int not null primary key,
		tipo varchar(30))

CREATE TABLE USUARIO(
		id_usuario int not null primary key,
		nombre varchar(30) not null,
		pass varchar(20) not null,
		DPI varchar(30),
		Correo varchar(30),
		Telefono int)

CREATE TABLE SOLICITUD(
		Correlativo int not null primary key,
		monto float,
		estado int)
		
CREATE TABLE RECIBO(
		idRecibo int not null primary key,
		pago float)
		
CREATE TABLE RECORRIDO(
		idRecorrido int not null primary key,
		fecha_inicio Date,
		usuario_idUsuario int,
		fecha_end Date,
		nombre Varchar(80))
	
CREATE TABLE SERVICIO(
		idServicio int not null primary key,
		Empresa_idEmpresa int,
		Tipo Varchar(25))
		
CREATE TABLE EVALUACION(
		estado int not null primary key,
		Evaluacion_idEmpresa int,
		Recorrido_idRecorrido int,
		detalle varchar(30))

CREATE TABLE EMPRESA(
		idEmpresa int not null primary key,
		Tipo_id_empresa int,
		nombre varchar(100),
		foto blob,
		direccion varchar(60),
		telefono int,
		correo varchar(60),
		detalle varchar(60),
		horario time,
		tarifa float,
		Especialidad integer)
		
Create Table Tipo(
		id_tipo int not null primary key,
		tipo varchar(20))
		
Create Table Sitio_turistico(
		id_sitio int not null primary key,
		Sitio_idEmpresa int,
		Sitio_idregion int,
		nombre varchar(100),
		imagen blob
)

CREATE TABLE REGION(
		id_region int not null primary key,
		tipo Varchar(20))

Alter table USUARIO add usuario_idRol int not null;
Alter Table SOLICITUD add solicitud_idEmpresa int not null;
Alter Table SOLICITUD add Solicitud_idUsuario int not null;
Alter Table Recibo add Recibo_idUsuario int not null;
Alter Table Recibo add Recibo_correlativo int not null;
		
ALTER TABLE USUARIO
			add constraint FOREIGN KEY(usuario_idRol) references rol(id_rol);
			
ALTER TABLE SOLICITUD
			add constraint FOREIGN KEY(solicitud_idEmpresa) references empresa(idempresa);

ALTER TABLE SOLICITUD
			add constraint FOREIGN KEY(Solicitud_idUsuario) references usuario(id_usuario);
			
ALTER TABLE RECIBO
			add constraint FOREIGN KEY(Recibo_idUsuario) references usuario(id_usuario);
ALTER TABLE RECIBO
			add constraint FOREIGN KEY(Recibo_Correlativo) references solicitud(correlativo);
			
ALTER TABLE Recorrido
			add constraint FOREIGN KEY(Usuario_idUsuario) references usuario(id_usuario);		

ALTER TABLE SERVICIO
			add constraint FOREIGN KEY(Empresa_idEmpresa) references Empresa(idempresa);	
ALTER TABLE Evaluacion
			add constraint FOREIGN KEY(Evaluacion_idEmpresa) references Empresa(idempresa);
			
ALTER TABLE EVALUACION
			add constraint FOREIGN KEY(Recorrido_idRecorrido) references Recorrido(idRecorrido);
			
ALTER TABLE Sitio_Turistico
			add constraint FOREIGN KEY(Sitio_idEmpresa) references Empresa(idEmpresa);
ALTER TABLE Sitio_Turistico
			add constraint FOREIGN KEY(Sitio_idRegion) references Region(id_Region);

ALTER TABLE Empresa
			add constraint FOREIGN KEY(Tipo_id_empresa) references tipo(id_tipo);